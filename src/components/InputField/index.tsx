import React from 'react'

type FieldProps = {
  className?: string
  inputStyle?: React.CSSProperties
  labelStyle?: React.CSSProperties
  id: string
  type?: string
  name: string
  label?: string
  prefix?: React.ReactNode
  placeholder?: string
  suffix?: React.ReactNode
  register: any
  onChange: (value: any) => void
  value?: string
}

function InputField(props: FieldProps) {
  const {
    id,
    type,
    label,
    prefix,
    suffix,
    name,
    placeholder,
    register,
    className,
    inputStyle,
    labelStyle,
    onChange,
    value
  } = props

  return (
    <div>
      {label && <label htmlFor={id}>{label}</label>}
      <div className={`${(prefix || suffix) && 'relative'}`}>
        {prefix && (
          <span
            className="mr-6 absolute top-0 left-6 dark:text-dark-text"
            style={{ ...labelStyle, transform: 'translateY(calc(50%))' }}
          >
            {prefix}
          </span>
        )}

        {suffix && (
          <span className="ml-6 absolute top-0 dark:text-dark-text">
            {suffix}
          </span>
        )}

        <input
          className={`${className ? className : ''} ${
            prefix ? 'px-14' : 'px-6'
          } bg-light-elem dark:bg-dark-elem rounded-md placeholder-light-input dark:placeholder-dark-text shadow-lg`}
          style={{ width: 480, height: 55, ...inputStyle }}
          {...{
            id,
            type: type || '',
            name,
            ref: register,
            placeholder,
            onChange,
            value
          }}
        />
      </div>
    </div>
  )
}

export default InputField
