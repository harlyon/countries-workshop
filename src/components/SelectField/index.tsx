import React from 'react'

type SelectFieldProps = {
  id?: string
  className?: string
  selectStyle?: React.CSSProperties
  name?: string
  label?: string
  onChange: (value: any) => void
  register?: any
  options?: Array<OptionProps>
  value?: string
}

type OptionProps = {
  value: string
  label: string
}

function SelectField(props: SelectFieldProps) {
  const {
    id,
    className,
    selectStyle,
    name,
    label,
    onChange,
    register,
    options,
    value
  } = props

  return (
    <div>
      {label && <label htmlFor={id}>{label}</label>}
      <select
        id={id}
        name={name}
        ref={register}
        style={{ height: 55, ...selectStyle }}
        className={` px-6 bg-light-elem dark:bg-dark-elem text-light-text dark:text-dark-text shadow-md rounded-md ${
          className ? className : ''
        }`}
        onChange={onChange}
        value={value}
      >
        {options?.map((opt: OptionProps) => (
          <option key={opt.value} value={opt.value}>
            {opt.label}
          </option>
        ))}
      </select>
    </div>
  )
}

export default SelectField
