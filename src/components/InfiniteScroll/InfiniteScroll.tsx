import React from 'react'

type InfiniteScrollProps<T> = {
  items: Array<T>
  renderItem: (
    itemProps: {
      item: T
      idx: number
      array: Array<T>
      key: string | number
    },
  ) => React.ReactNode
  renderEmptyList?: () => React.ReactNode
  keyExtractor: (item: T, idx: number) => string
  className?: string
}

function InfiniteScroll<T>({
  items,
  keyExtractor,
  renderItem,
  renderEmptyList,
  className
}: InfiniteScrollProps<T>) {

  return (
    <div className={`${className ? className: ''} infinite-scroll`}>
      {renderEmptyList && !items?.length && renderEmptyList()}
      {items?.map((item, idx, array) => {
        return renderItem({ item, idx, array, key: keyExtractor(item, idx) })
      })}
    </div>
  )
}

export default InfiniteScroll
