import React from 'react'
import InputField from '../InputField'
import SelectField from '../SelectField'
import './filter-form.css'

type FilterFormProps = {
  className?: string
  onSubmit: any
  register: any
  onChangeSearchField: any
  onChangeRegion: any
  searchFieldValue?: string
  regionValue?: string
}

function FilterForm({
  className,
  onSubmit,
  register,
  onChangeSearchField,
  onChangeRegion,
  searchFieldValue,
  regionValue
}: FilterFormProps) {
  return (
    <form onSubmit={onSubmit}>
      <div className="flex flex-col lg:flex-row lg:justify-between">
        <InputField
          id="input-search-country"
          className={className}
          name="search-country"
          prefix={<i className="fas fa-search" />}
          placeholder="Search for a country..."
          register={register}
          inputStyle={{ width: '100%', maxWidth: 480 }}
          onChange={onChangeSearchField}
          value={searchFieldValue}
        />
        <SelectField
          name="region"
          className={`input-select-region mt-12 lg:mt-0 ${regionValue !== '' ? 'dark:text-light-text' : ''}`}
          register={register}
          onChange={onChangeRegion}
          selectStyle={{}}
          value={regionValue}
          options={[
            {
              value: '',
              label: 'Filter by region...'
            },
            { value: 'Africa', label: 'Africa' },
            { value: 'Americas', label: 'America' },
            { value: 'Asia', label: 'Asia' },
            { value: 'Europe', label: 'Europe' },
            { value: 'Oceania', label: 'Oceania' }
          ]}
        />
      </div>
    </form>
  )
}

export default FilterForm
