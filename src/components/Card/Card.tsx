import React from 'react'

type CardProps = {
  className?: string
  style?: React.CSSProperties
  children: React.ReactNode
  onClick: any
}

function Card(props: CardProps) {
  const { className, style, children, onClick } = props
  return (
    <div
      className={`${
        className ? className : ''
      } bg-light-elem dark:bg-dark-elem rounded-lg overflow-hidden shadow-lg hover:shadow-xl cursor-pointer`}
      {...{ onClick, style }}
    >
      {children}
    </div>
  )
}

export default Card
