import React from 'react'

type ContentProps = {
  children: React.ReactNode
}

function Content({ children }: ContentProps) {
  return <article className="py-8 sm:py-10 px-4 sm:px-20">{children}</article>
}

export default Content
