import React, { useContext } from 'react'
import { ThemeContext } from '../../contexts/ThemeContext'
import { Link } from 'react-router-dom'

function Header() {
  const { theme, setTheme } = useContext(ThemeContext)

  const isDark = theme === 'dark'

  function handleDarkMode() {
    setTheme && setTheme(isDark ? 'light' : 'dark')
  }
  return (
    <header
      className="header flex justify-between items-center px-4 sm:px-20 text-light-text dark:text-dark-text shadow-lg"
      style={{ height: 80 }}
    >
      <h1 className="text-lg sm:text-3xl">
        <Link to="/">Where in the world?</Link>
      </h1>
      <div
        className="cursor-pointer flex items-center"
        onClick={handleDarkMode}
      >
        <span className="mr-2.5">
          <i className={`fas fa-${isDark ? 'sun' : 'moon'} text-2xl`} />
        </span>
        <span>{isDark ? 'Light' : 'Dark'} Mode</span>
      </div>
    </header>
  )
}

export default Header
