import axios from 'axios'

const BASE_URL = 'https://restcountries.eu/rest/v2'

export async function findCountries(params?: {
  searchField?: string
  region?: string
}) {
  if (params?.searchField || params?.region) {
    return await axios.get(`${BASE_URL}/name/${params.searchField}`)
  }
  return await axios.get(`${BASE_URL}/all`)
}

export async function findById(id: string) {
  return await axios.get(`${BASE_URL}/alpha/${id}`)
}

export async function findByRegion(region: string) {
  return await axios.get(`${BASE_URL}/region/${region}`)
}
