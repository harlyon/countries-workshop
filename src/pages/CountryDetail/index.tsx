import React, { useEffect, useState } from 'react'
import { useHistory, useParams } from 'react-router'
import * as ApiCountries from '../../services/APIs/countries.api'
import LoadingIndicator from '../../components/LoadingIndicator'
import './country-detail.css'

const CountryDetail = () => {
  const history = useHistory()
  const params: { id: string } = useParams()

  const [countries, setCountries] = useState<Country[]>([])
  const [country, setCountry] = useState<Country>({} as Country)
  const [isLoading, setIsLoading] = useState(false)
  const [error, setError] = useState('')

  useEffect(() => {
    setIsLoading(true)
    ;(async () => {
      try {
        const { data: countries } = await ApiCountries.findCountries()
        const foundCountry = countries?.find((c: Country) => {
          return params.id === c.alpha3Code
        })
        setCountry(foundCountry)
        setCountries(countries)
        document.title = foundCountry?.name
      } catch (err) {
        setError(err?.response?.data?.message)
        console.error('❌ Error', err)
      } finally {
        setIsLoading(false)
      }
    })()
  }, [params.id])

  return (
    <div className="country-detail-page text-light-text dark:text-dark-text">
      <button className="bg-light-elem dark:bg-dark-elem px-4 py-0.5 rounded inline-flex items-center mb-12 shadow-md">
        <i className="fas fa-long-arrow-alt-left" />
        &nbsp;
        <span className="font-thin" onClick={() => history.goBack()}>
          Back
        </span>
      </button>
      {isLoading && !country?.name ? (
        <LoadingIndicator />
      ) : !error ? (
        <div className="country-page-container flex flex-col lg:flex-row gap-10">
          <img
            alt={country?.name}
            src={country?.flag}
            className="mb-10 h-auto w-full lg:w-6/12 lg:max-h-80"
          />
          <div className="text-zone">
            <div className="country-title text-lg">
              <span className="font-extrabold mb-8">{country?.name}</span>
            </div>

            <div className="country-detail text-sm flex flex-col lg:flex-row gap-4 lg:gap-10 mt-6">
              <div className="general-detail">
                <div className="each-detail">
                  <span className="label">Native Name:&nbsp;</span>
                  <span>{country?.nativeName}</span>
                </div>
                <div className="each-detail">
                  <span className="label">Population:&nbsp;</span>
                  <span>{country?.population?.toLocaleString()}</span>
                </div>
                <div className="each-detail">
                  <span className="label">Region:&nbsp;</span>
                  <span>{country?.region}</span>
                </div>
                <div className="each-detail">
                  <span className="label">Sub Region:&nbsp;</span>
                  <span>{country?.subregion}</span>
                </div>
              </div>

              <div className="another-detail mt-4 lg:mt-0">
                <div className="each-detail">
                  <span className="label">Top Level Domain:&nbsp;</span>
                  <span>{country?.topLevelDomain}</span>
                </div>
                <div className="each-detail">
                  <span className="label">Currencies:&nbsp;</span>
                  <span>
                    {country?.currencies?.reduce((acc, curr, idx) => {
                      return idx < country.currencies.length - 1
                        ? `${acc}${curr.name}, `
                        : `${acc}${curr.name}`
                    }, '')}
                  </span>
                </div>
                <div className="each-detail">
                  <span className="label">Languages:&nbsp;</span>
                  <span>
                    {country?.languages?.reduce((acc, curr, idx) => {
                      return idx < country.languages.length - 1
                        ? `${acc}${curr.name}, `
                        : `${acc}${curr.name}`
                    }, '')}
                  </span>
                </div>
              </div>
            </div>

            <div className="border-detail mt-10">
              <div className="each-detail">
                <span className="label">Border Countries</span>
                <div className="flex flex-wrap">
                  {country?.borders
                    ?.map((border) => {
                      const foundCountry = countries?.find((c) => {
                        return border === c.alpha3Code
                      })
                      return {
                        name: foundCountry?.name,
                        key: foundCountry?.alpha3Code
                      }
                    })
                    .map(({ name, key }) => (
                      <button
                        key={key}
                        onClick={() => history.push(`/countries/${key}`)}
                        className="btn-country-border bg-light-elem dark:bg-dark-elem shadow-md px-2 mr-2 mb-2 rounded min-w-20"
                      >
                        {name}
                      </button>
                    ))}
                </div>
              </div>
            </div>
          </div>
        </div>
      ) : (
        <div className="text-error text-2xl">{error}</div>
      )}
    </div>
  )
}

export default CountryDetail
