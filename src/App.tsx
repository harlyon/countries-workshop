import React, { Suspense } from 'react'
import './App.css'
import Layout from './components/Layout'
import ThemeProvider from './contexts/ThemeContext'
import {
  BrowserRouter as Router,
  Redirect,
  Route,
  Switch
} from 'react-router-dom'
import Notfound from './pages/Notfound'
import LoadingIndicator from './components/LoadingIndicator'

const Countries = React.lazy(() => import('./pages/Countries'))
const CountryDetail = React.lazy(() => import('./pages/CountryDetail'))

const routes = [
  { path: '/', component: Countries },
  { path: '/countries/:id', component: CountryDetail },
  { path: '/notfound', component: Notfound }
]

function App() {
  return (
    <div className="dark:bg-dark-bg bg-light-bg">
      <ThemeProvider>
        <Router>
          <Layout>
            <Suspense fallback={<LoadingIndicator />}>
              {/*<Countries />*/}
              <Switch>
                {routes.map(({ path, component, ...props }) => (
                  <Route
                    exact={true}
                    key={path}
                    path={path}
                    component={component}
                    {...props}
                  />
                ))}
                <Redirect to="/notfound" />
              </Switch>
            </Suspense>
          </Layout>
        </Router>
      </ThemeProvider>
    </div>
  )
}

export default App
