module.exports = {
  purge: ['./src/**/*.{js,jsx,ts,tsx}', './public/index.html'],
  darkMode: 'class', // or 'media' or 'class'
  theme: {
    colors: {
      transparent: 'transparent',
      current: 'currentColor',
      dark: {
        elem: '#2B3945',
        bg: '#202C37',
        text: '#FFFFFF'
      },
      light: {
        elem: '#FFFFFF',
        bg: '#FAFAFA',
        text: '#111517',
        input: '#858585',
      },
      error: '#FF0909'
    },
    extend: {},
  },
  variants: {
    extend: {},
  },
  plugins: [],
}
