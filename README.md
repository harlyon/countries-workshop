# 🏁🏴 REST Countries API with color theme switcher workshop 🏴🏁

### 🏁 About this challenge 🏁
[Frontend Mentor - Countries challenge](https://www.frontendmentor.io/challenges/rest-countries-api-with-color-theme-switcher-5cacc469fec04111f7b848ca)  
- [My Submission](https://www.frontendmentor.io/solutions/react-typescript-with-hook-tailwindcss-reacthook-form-RaxuArVcK)  
- [Preview Site](https://countries-workshop.vercel.app)  

#### 📷 Requirement Pics 🖼

![Desktop Preview](./rest-countries-api-with-color-theme-switcher-master/design/desktop-preview.jpg)  

![Desktop List Light](./rest-countries-api-with-color-theme-switcher-master/design/desktop-design-home-light.jpg)  

![Desktop List Dark](./rest-countries-api-with-color-theme-switcher-master/design/desktop-design-home-dark.jpg)  

![Desktop Detail Light](./rest-countries-api-with-color-theme-switcher-master/design/desktop-design-detail-light.jpg)  

![Desktop Detail Dark](./rest-countries-api-with-color-theme-switcher-master/design/desktop-design-detail-dark.jpg)    

![Mobile List Light](./rest-countries-api-with-color-theme-switcher-master/design/mobile-design-home-light.jpg)        

![Mobile List Dark](./rest-countries-api-with-color-theme-switcher-master/design/mobile-design-home-dark.jpg)          

![Mobile Detail Light](./rest-countries-api-with-color-theme-switcher-master/design/mobile-design-detail-light.jpg)    

![Mobile Detail Dight](./rest-countries-api-with-color-theme-switcher-master/design/mobile-design-detail-dark.jpg)     
